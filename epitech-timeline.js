'use strict';

const now = new Date;

// Catch all localStorage errors early to avoid it if unavailable
try {
  var storage = localStorage;
  storage.bttf = storage.bttf;
  storage.dark = storage.dark;
} catch (_) {
  storage = {};
}


/* Timeline */

const timelineData = fetch('data.json', { cache: 'no-cache' })
  .then(r => r.ok ? r.json() : Promise.reject(r.statusText))
  .then(({ promo, semester, projects }) => {
    document.getElementById('title')
      .textContent = document.title =
        `Epitech Timeline - Promotion ${promo}`;
    document.getElementById('semester')
      .textContent = `Semester ${semester}`;
    return projects;
  }, e => {
    throw document.getElementById('timeline-container')
      .textContent = `Timeline data fetching failed: ${e}`;
  });

function shapeTimeline() {
  // Remove remaining tooltips left over on timeline redraw
  const tooltips = document
    .getElementsByClassName('google-visualization-tooltip');
  [].forEach.call(tooltips, tooltip => tooltip.remove());

  // One-shot js styling to avoid glitches introduced by global CSS rules
  const [ nowRow, nowLine ] = document.querySelectorAll('rect:first-child');
  const nowText = nowLine.nextSibling;
  const timelineRect = nowLine.parentElement.getBoundingClientRect();

  nowRow.style.setProperty('fill', 'none');
  nowLine.style.setProperty('fill', '#8888');
  nowLine.style.setProperty('pointer-events', 'none');
  nowText.style.setProperty('pointer-events', 'none');
  nowLine.style.setProperty('height', timelineRect.height);
}

google.charts.load('current', { packages: [ 'timeline' ] }).then(async () => {
  const timeline = new google.visualization.ChartWrapper({
    chartType: 'Timeline',
    containerId: 'timeline-container',
    options: { timeline: { colorByRowLabel: true } },
    dataTable: [
      [ 'Module', 'Project', 'BTTF', 'Start', 'End' ],
      [ '—', 'Now', false, now, now ], // Now row
    ].concat((await timelineData)
        .map(({ module, project, bttf, start, end }) =>
          [ module, project, bttf, new Date(start), new Date(end) ]
      ))
  });

  const columns = [ 0, 1, 3, 4 ]; // Hide bttf-ness (column 2)
  const nonBttfRows = timeline.getDataTable()
    .getFilteredRows([{ column: 2, value: false }]);

  (function toggleBttf(showBttf) {
    const bttfButton = document.getElementById('bttf-button');
    bttfButton.textContent = `${showBttf ? 'Hide' : 'Show'} BTTFs`;
    bttfButton.onclick = () => (toggleBttf(storage.bttf = !showBttf), false);

    timeline.setView(showBttf ? { columns } : { columns, rows: nonBttfRows });
    (onresize = () => { timeline.draw(); shapeTimeline(); })();
  })(storage.bttf === 'true');

  // Disable cell selection
  (function unselectAll(listener) {
    google.visualization.events.removeListener(listener);
    timeline.getChart().setSelection([]);
    listener = google.visualization.events
      .addListener(timeline, 'select', () => unselectAll(listener));
  })();
});


/* Changelog */

const timeUnits = [
  [  1, 'second' ],
  [ 60, 'minute' ],
  [ 60, 'hour'   ],
  [ 24, 'day'    ],
  [ 30, 'month'  ],
  [ 12, 'year'   ],
].map((factor => ([ coef, unit ]) => [ factor *= coef, unit ])(1)).reverse();

function makeCommitHtml(url, [ title, ...description ], date) {
  let elapsed = (now - date) / 1000;
  const [ timeCoef, timeUnit ] = timeUnits.find(([ coef ]) => coef < elapsed);
  elapsed = Math.round(elapsed / timeCoef);

  return `
    <p class="commit ${description.length > 0 ? 'expanded' : ''}">
      <a class="commit-msg" href="${url}" target="_blank">${title}</a>
      ${description.map(line =>
        `<span class="commit-desc">${line}</span>`
      ).join('')}
      <span class="commit-date" title="${date}">
        ${elapsed} ${timeUnit}${elapsed === 1 ? '' : 's'} ago
      </span>
    </p>
  `;
}

const project = encodeURIComponent('epi-codes/Epitech-2023-Timeline');
fetch(`https://gitlab.com/api/v4/projects/${project}/repository/commits`)
  .then(async r => r.ok ? r.json() : Promise.reject(await r.json()))
  .then(commits => commits.map(({ web_url, message, created_at }) => {
    const splitMsg = message.split('\n')
      .filter(({ length }) => length > 0);
    return makeCommitHtml(web_url, splitMsg, new Date(created_at));
  }), e => [ `Gitlab commits fetching failed: ${e.error}` ])
  .then(html => {
    document.getElementById('changelog-container')
      .innerHTML = html.join('');
  });


/* Dark theme */

function toggleDarkTheme() {
  const isDark = storage.dark =
    document.body.classList.toggle('dark');
  document.getElementById('theme-button')
    .textContent = `Switch to ${isDark ? 'light' : 'dark'}`;
}

if (storage.dark === 'true') {
  toggleDarkTheme();
}
